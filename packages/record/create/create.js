
const fetch = require('node-fetch')
const faunadb = require('faunadb')
const q = faunadb.query
const client = new faunadb.Client({
  secret: process.env.FAUNADB_SECRET,
  endpoint: "https://db.fauna.com/",
  fetch
} )
const DIGITALOCEAN_ENDPOINT = "https://api.digitalocean.com/v2"
const DIGITALOCEAN_TOKEN = process.env.DIGITALOCEAN_TOKEN
const TURNSTILE_ENDPOINT = "https://challenges.cloudflare.com/turnstile/v0/siteverify"
const TURNSTILE_SECRET = process.env.TURNSTILE_SECRET
const ZONE = process.env.ZONE

function response(data, status=200){
  return { "body": JSON.stringify(data), "statusCode": status, "headers": { "Content-Type": "application/json" } }
}

function minLength(word, l){
  return word.length < l
}

function validateHostname(name){
  const matches = name.match(/^[a-z0-9]+((\-[a-z0-9]+|\.[a-z0-9]+)+)?/gi)
  return matches && matches.length > 0 ? matches[0].toLowerCase() : false
}

function validateIpAddress(ip){
  const matches = ip.match(/(\b25[0-5]|\b2[0-4][0-9]|\b[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}/gm)
  return matches && matches.length > 0 ? matches[0] : false
}

async function send(action, data = null, method = "GET"){

  let options =  {
    headers: {
      "Authorization": `Bearer ${ DIGITALOCEAN_TOKEN }`,
      "Content-Type": "application/json",
      "Accept": "application/json"
    },
    method: method.toUpperCase()
  }

  if( ! ["HEAD", "GET"].includes(options.method) ){
    options.body = JSON.stringify(data)
  }
  const url = `${ DIGITALOCEAN_ENDPOINT }/domains/${ ZONE }/${ action }`
  const { Authorization, ...withoutAuthorization } = options.headers
  const { headers, ...withoutHeaders } = options
  console.log({ 
    "digitalocean-request": JSON.stringify({
      url,
      options: {
        headers: withoutAuthorization,
        ...withoutHeaders
      }
    })
  })
  const response = await fetch( url, options )
  const result = await response.json()
  console.log({ "digitalocean-result": JSON.stringify(result) })

  return result

}

async function captcha( token ){
  const data = {
    secret: TURNSTILE_SECRET,
    response: token
  }
  console.log({
    "captcha-request": JSON.stringify({
      url: TURNSTILE_ENDPOINT,
      token: data.token
    })
  })
  const response = await fetch(TURNSTILE_ENDPOINT, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json"
    },
    body: JSON.stringify(data)
  })
  const result = await response.json()
  console.log({ "captcha-result": JSON.stringify(result) })
  return result;
}

async function getByFullName(name) {
  return await send(`records?type=A&name=${name}`)
}

async function getById(id){
  return await send(`records/${id}`)
}

async function create(name, content) {
  const record = {
    type: "A",
    name: name,
    data: content,
    ttl: 300
  }
  return await send("records", record, "POST")
}

async function destroy(id){
  return await send(`records/${id}`, null, "DELETE")
}

async function recordExists(name, zone){
  try {
    const { data } = await client.query(
      q.Get( q.Match( q.Index("ByHostname"), [ name, zone ] ) )
    )
    console.log({ data })
    return data
  } catch(err) {
    return false
  }
}

exports.main = async (event) => {
  const { name, content, turnstile, http } = event

  if( typeof name != "string" || typeof content != "string" || typeof turnstile != "string" ){
    return response({ success: false, errors: [ "Some fields is incomplete or invalid" ] }, 422)
  }

  const hostname = validateHostname(name)
  const ip = validateIpAddress(content)

  if( hostname === false ){
    return response({ success: false, errors: [ "Hostname is invalid" ] }, 422)
  }
  if( minLength( hostname, 4 ) ){
    return response({ success: false, errors: [ "Hostname has to be atleast 4 characters" ] }, 422)
  }
  if( ip === false ){
    return response({ success: false, errors: [ "IP is invalid" ] }, 422)
  }

  const notarobot = await captcha( turnstile )

  if( ! [
      "1x0000000000000000000000000000000AA", "2x0000000000000000000000000000000AA", "3x0000000000000000000000000000000AA"
    ].includes(TURNSTILE_SECRET) && notarobot.action != "dns-create" ){
    return response({ success: false, errors: [ "Expected captcha action does not match" ] }, 422)
  }
  if( ! notarobot.success ){
    return response({ success: false, errors: [ "Fail captcha validation" ] }, 422)
  }

  if( await recordExists( hostname, ZONE ) ){
    return response({ success: false, errors:[ "Hostname already exists" ] }, 422)
  }

  const matches = await getByFullName(`${ hostname }.${ ZONE }`)

  if(matches.meta.total > 0){
     return response({ success: false, errors: [ "Hostname already exists" ] }, 422)
  }
  try {
    const created = await create(hostname, ip)
    const record = created["domain_record"]
    // create document
    await client.query(
      q.Create(
        q.Collection("records"),
        {
          data: {
            id: record.id,
            name: record.name,
            content: record.data,
            type: record.type,
            ttl: record.ttl,
            zone: ZONE,
            provider: "digitalocean",
            "user-agent": http.headers["user-agent"],
            "ip-address": http.headers["x-forwarded-for"]
          }
        }
      )
    )
    return response({ success: true, record }, 201)
  } catch ( err ){
    return response({ success: false, errors: [ err.message ] }, 500)
  }
}

