import Home from '@/views/Home.vue'

const routes = [
  {
    path: '/',
    name: "home",
    component: Home
  },
  {
    path: '/extend',
    name: "extend",
    component: () => import("@/views/Extend.vue")
  },
  {
    path: '/:pathMatch(.*)*',
    name: "404",
    component: () => import("@/views/404.vue")
  }
]

export default routes
